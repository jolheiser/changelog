module code.gitea.io/changelog

go 1.13

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/google/go-github v17.0.0+incompatible
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/urfave/cli/v2 v2.1.1
	gopkg.in/yaml.v2 v2.2.7
)
